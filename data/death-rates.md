# CDC Vaccination Data
This data seeks to compare Macon County's Death rate to the death rate in surrounding counties.

This data was prepared by Jim Spaniol. 

## Data Source
I was provided this data by Jim Spaniol. Jim used the sources linked below.

## Links
- [idph covid19 statistics](https://www.dph.illinois.gov/covid19/statistics) 
- [worldpopulationreview.com](https://worldpopulationreview.com/us-counties/states/il)

## Notes about this data set
- The cases / tests / deaths are for the week of 9/24/2021 & 9/17/2021.

## Other Notes
- FIPS code for Macon County: 17715
- This was originally discussed and presented [on Facebook](https://www.facebook.com/LoveDecatur/posts/390180935821197)


