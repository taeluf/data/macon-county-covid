# Daily Case, Death, Test Counts | Macon County Department of Health
This data counts the number of cases, deaths & tests in Macon County

## Data Source 
This data comes from Macon County Health Department. I look at their daily case-number infographics on facebook & write the numbers into a spreadsheet.

## Links
- [MCHD Facebook](https://www.facebook.com/Macon-County-Health-Department-Decatur-IL-116643335063685/)

## Notes about this data set
- The weekly releases MCHD puts out on Friday are NOT included in these 'daily' numbers. Instead, the following Monday's daily report is used, which cites the daily case numbers for Saturday, Sunday, and Monday. Friday/Saturday/Sunday numbers are calculated by subtracting the new daily case numbers from the monday total.

## General MCHD Data Reporting Notes
- I have not collected case numbers throughout the pandemic, so I only have limited data available. 
- During periods of the pandemic, MCHD stopped posting upates
- MCHD also used to post the infographics to their website. They stopped in August 2021.
- MCHD does NOT make data available in spreadsheet format.
- I have made FOIA requests for COVID data in a raw format, and my request was denied. They claimed it was unduly burdensome. I've heard from one other person who had a similar experience.
