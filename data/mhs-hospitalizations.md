# Hospitalizations | Memorial Health Systems (MHS)
This data tracks hospitalizations due to COVID in HSHS hospitals. It breaks down hospitalizations by number of folks hospitalized vs in the icu vs on ventilators. It breaks down vaccinated vs unvaccinated

## Data source 
HSHS releases infographics containing hospitalization data. I write the numbers into a spreadsheet.

"The dashboard includes the number of people currently hospitalized at Decatur Memorial Hospital, Jacksonville Memorial Hospital, Lincoln Memorial Hospital, Springfield Memorial Hospital and Taylorville Memorial Hospital."
- From the Oct 1 facebook post

### Links
- [MHS Facebook](https://www.facebook.com/ChooseMemorial/)
- [New mhs website](https://memorial.health/medical-services/covid-19-information) - only shows the latest infographic
- The [old mhs website](https://www.choosememorial.org/COVID19) was moved to `https://memorial.health` & this link no longer works. You can always look at old copies of the page on [archive.org](https://web.archive.org/web/*/https://www.choosememorial.org/COVID19)

## Notes
- October 1, 2021: The old mhs website was taken down & MHS now uses `https://memorial.health`. [Old mhs website](https://www.choosememorial.org/COVID19) was moved to `https://memorial.health`.

