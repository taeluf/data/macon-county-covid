# CDC Vaccination Data
This data counts the number of people vaccinated againsit COVID in Macon County.

## Data Source
I copy the data from CDC's data

### Links
- [main data page](https://data.cdc.gov/Vaccinations/COVID-19-Vaccinations-in-the-United-States-County/8xkx-amqh)
- [download as .csv](https://data.cdc.gov/resource/8xkx-amqh.csv?fips=17115)
- [download as .json](https://data.cdc.gov/resource/8xkx-amqh.json?fips=17115)

## Notes
- FIPS code for Macon County: 17715
- The Macon County Population estimate is 104,009 people. I estimate about 15,000 children under 12 (see below)
- The [Macon County population estimate](https://covid.cdc.gov/covid-data-tracker/#county-view|Risk|community_transmission_level) is 104,009 as of Sept 11, 2021, according to the CDC

## Notes
- The CDC added a few columns in late September or early October, 2021. I have the new data, but have not included them in the data conversions to other formats.

## Population Info
- Population Estimate: The [Macon County population estimate](https://covid.cdc.gov/covid-data-tracker/#county-view|Risk|community_transmission_level) is 104,009 as of Sept 11, 2021, according to the CDC
- [2019 Census Data](https://www.census.gov/quickfacts/fact/table/maconcountyillinois/PST045219) shows 104,009 people in Macon County. The cdc uses [census population estimates](https://www.census.gov/data/datasets/time-series/demo/popest/2010s-counties-total.html) & their site also shows 104,009 people. From census data, I estimate about 15,000 children under the age of 12 (who are not covid-vaccine eligible)
