# Hospitalizations | Hospital Sisters Health Systems (hshs)
This data tracks hospitalizations due to COVID in HSHS hospitals. It breaks down hospitalizations by number of folks hospitalized vs in the icu vs on ventilators. It breaks down vaccinated vs unvaccinated

## Data Source
HSHS releases infographics containing hospitalization data. I write the numbers into a spreadsheet.

"Total documented positive COVID-19 cases in nine HSHS Illinois Hospitals and six HSHS Wisconsin Hospitals" - from the reports

### Links
- [HSHS Facebook](https://www.facebook.com/HospitalSistersHealthSystem) 

## Notes
- August 30th: "Age 65 or younger" stat for hospitalizations was added to infographic
- September 7th: 
    - "Age 65 or younger" is changed to "under age 65" 
    - "Under Age 65" is now broken down by 'vaccinated folks under 65' & 'unvaccinated folks under 65'
- October 12th, 2021: There hasn't been a new infographic since September 20th
