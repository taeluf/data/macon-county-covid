# MCHD Facebook Posts text content
This will be an exact copy of the text from MCHD's facebook posts

## Oct 19, 2021
Since the Macon County Health Department’s last update on Monday, October 18th, 2021, there have been 30 new COVID-19 cases reported in Macon County. This brings the total number of COVID-19 cases to 15,395 since the start of the pandemic. There are currently 9 hospitalizations.
The full infographic including all demographic information will be released on Friday. Please do everything you can to prevent the spread of this virus and reduce the strain on our local health care system.


## OCt 20, 2021
COVID-19 Update
October 20, 2021

Since the Macon County Health Department’s last update on Tuesday, October 19, 2021, there have been 17 new COVID-19 cases reported in Macon County. This brings the total number of COVID-19 cases to 15,412 since the start of the pandemic. There are currently 9 hospitalizations.

The full infographic including all demographic information will be released on Friday. Please do everything you can to prevent the spread of this virus and reduce the strain on our local health care system.

## Oct 21, 2021
Since the Macon County Health Department’s last update on Wednesday, October 20, 2021, there have been 15 new COVID-19 cases reported in Macon County. This brings the total number of COVID-19 cases to 15,427 since the start of the pandemic. There are currently 10 hospitalizations. We are incredibly saddened that we also must report the passing of one resident with COVID-19 during this timeframe. This resident was a male in his 60’s.

The full infographic including all demographic information will be released on Friday. Please do everything you can to prevent the spread of this virus and reduce the strain on our local health care system.

## November 2, 2021
https://www.facebook.com/116643335063685/photos/a.136673823060636/4515065131888128/
Since the Macon County Health Department’s last update on Monday, November 1, 2021, there have been 24 new COVID-19 cases reported in Macon County. This brings the total number of COVID-19 cases to 15,668 since the start of the pandemic. There are currently 7 hospitalizations.
Please note that, while there were 24 newly-reported cases, 2 previously-reported cases were determined to be from out of county and were transferred appropriately
The full infographic including all demographic information will be released on Friday. Please do everything you can to prevent the spread of this virus and reduce the strain on our local health care system.

## 10/3/2021
https://www.facebook.com/116643335063685/photos/a.136673823060636/4518241344903840/
Since the Macon County Health Department’s last update on Tuesday, November 2, 2021, there have been 31 new COVID-19 cases reported in Macon County. This brings the total number of COVID-19 cases to 15,699 since the start of the pandemic. There are currently 10 hospitalizations. 
The full infographic including all demographic information will be released on Friday. Please do everything you can to prevent the spread of this virus and reduce the strain on our local health care system.

## November 4, 2021
COVID-19 Daily Update
November 4, 2021
Since the Macon County Health Department’s last update on Wednesday, November 3, 2021, there have been 31 new COVID-19 cases reported in Macon County. This brings the total number of COVID-19 cases to 15,730 since the start of the pandemic. There are currently 10 hospitalizations.
The full infographic including all demographic information will be released on Friday. Please do everything you can to prevent the spread of this virus and reduce the strain on our local health care system.

## November 8, 2021
https://www.facebook.com/116643335063685/photos/a.136673823060636/4533982833329691/
November 8, 2021 
Since the Macon County Health Department’s last update on Friday, November 5, 2021, there have been 67 new COVID-19 cases reported in Macon County, bringing the total number of COVID-19 cases to 15,822 since the start of the pandemic. There were 15 newly-reported cases on Saturday, 13 newly-reported cases on Sunday, and 39 newly-reported cases on Monday. The total number of hospitalizations is 15. 
Please note that, while there were 67 newly-reported cases,1 previously-reported case was determined to be from out of county and was transferred appropriately.

## November 9, 2021 
Since the Macon County Health Department’s last update on Monday, November 8, 2021, there have been 42 new COVID-19 cases reported in Macon County, bringing the total number of COVID-19 cases to 15,864 since the start of the pandemic. The total number of hospitalizations is 15. 
The full infographic including all demographic information will be released on Friday. Please do everything you can to prevent the spread of this virus and reduce the strain on our local health care system.

## November 10, 2021
November 10, 2021 
COVID-19 Daily Update
Since the Macon County Health Department’s last update on Tuesday, November 9th, 2021, there have been 53 new COVID-19 cases reported in Macon County. This brings the total number of COVID-19 cases to 15,916 since the start of the pandemic. Please note that, while there were 53 newly-reported cases, 1 previously-reported cases was determined to be from out of county and was transferred appropriately. There are currently 15 hospitalizations. 
We are incredibly saddened that we also must report the passing of a resident with COVID-19 during this timeframe. The resident was a female in their 70’s.
The full infographic including all demographic information will be released on Friday. Please do everything you can to prevent the spread of this virus and reduce the strain on our local health care system.
