# COVID Data | Macon County, Illinois | Decatur, Illinois 
COVID data that has been collected from various sources. Most of it is specific to Macon County, IL. 

Check [LoveDecatur.com/covid/](https://lovedecatur.com/covid/) for the data in a friendlier format.

## Project Abandoned | March 3, 2022
I abandoned this project in November 2021. I don't intend to develop this further or provide up-to-date data. I do not intend to answer merge requests. If you wish to use it, please just fork & do your own thing. If you want to link to a fork or another dataset like this, please submit an issue & I'll add it here.

The website LoveDecatur.com does still have some covid updates, but i haven't open-sourced them because it's extra work.

Thank you for your interest. 

## Updates
I will update the data every Tuesday & Friday. This note was written on September 21, 2021. (This note will not be regularly updated)

## Data
You download data in the following formats:
- [.ods](/data/): spreadsheet for [libre office](https://www.libreoffice.org/) (a free alternative to Microsoft Office)
- [.csv](/data-csv/): Generic spreadsheet for any spreadsheet program
- [.json](/data-json/): A format often used by software developers
- [.sql](/data-sql/): A programming language used with databases. These scripts are for creating a database of the data
- [data.sqlite](/data/data.sqlite): The actual data compiled into a database file. Learn more [about sqlite](https://sqlite.org/index.html)

Some additional source documents will be available in the `/data/` folder, too. For Example, [/data/mhs/](/data/mhs/) contains image releases from Memorial Health Systems.

## Methodology
Most of the spreadsheets list the data source I'm collecting from. I manually convert from the source format into the spreadsheets I have. There is also a `data-sources.ods` file that lists some of my data sources.

There may be mistakes. It's just me, on a volunteer basis, and no-one is verifying the data I put in, so typos or other human errors may pop up.

---

## Software Developers
- The `.ods` conversion into all available formats is done with an [open source library](https://gitlab.com/taeluf/php/spreadsheet-to-db) I made. Feel free to use it for your needs.
- Let me know if you have questions or ... whatever. Open an issue or get me on Twitter @TaelufDev
- See [/README-Developers.md](/README-Developers.md) for more development information


