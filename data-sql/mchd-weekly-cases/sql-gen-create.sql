    DROP TABLE IF EXISTS `mchd_weekly_cases`;

    CREATE TABLE `mchd_weekly_cases`
    (
    `date` DATE, `url` VARCHAR(175), `cases` int(10), `released_from_isolation` int(10), `in_home_isolation` int(10), `hospitalized` int(10), `deaths` int(10), `male_cases` int(10), `female_cases` int(10), `cases_under_10` int(10), `cases_10_to_19` int(10), `cases_20_to_29` int(10), `cases_30_to_39` int(10), `cases_40_to_49` int(10), `cases_50_to_59` int(10), `cases_60_to_69` int(10), `cases_70_to_79` int(10), `cases_80_to_89` int(10), `cases_90_to_99` int(10), `cases_100_plus` int(10), `cases_black` int(10), `cases_asian` int(10), `cases_white` int(10), `cases_other_race` int(10), `cases_unknown_race` int(10), `cases_unknown_gender` int(10)
    )
    ;
    