    DROP TABLE IF EXISTS `mhs_hospitalizations`;

    CREATE TABLE `mhs_hospitalizations`
    (
    `date` DATE, `time` VARCHAR(61), `hospitalized` int(10), `novax_hosp` int(10), `vax_hosp` int(10), `icu` int(10), `novax_icu` int(10), `vax_icu` int(10), `ventilated` int(10), `novax_vent` int(10), `vax_vent` int(10), `novax_hosp_percent` FLOAT, `novax_icu_percent` FLOAT, `novax_vent_percent` FLOAT
    )
    ;
    