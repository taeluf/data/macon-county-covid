    DROP TABLE IF EXISTS `hshs_hospitalizations`;

    CREATE TABLE `hshs_hospitalizations`
    (
    `date` DATE, `time` VARCHAR(61), `hospitalized` int(10), `novax_hosp` int(10), `vax_hosp` int(10), `icu` int(10), `novax_icu` int(10), `vax_icu` int(10), `ventilated` int(10), `novax_vent` int(10), `vax_vent` int(10), `novax_hosp_percent` FLOAT, `novax_icu_percent` FLOAT, `novax_vent_percent` FLOAT, `age_65_or_younger` int(10), `age_65_or_younger_novax` int(10), `age_65_or_younger_vax` int(10)
    )
    ;
    