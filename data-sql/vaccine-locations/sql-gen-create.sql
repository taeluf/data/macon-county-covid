    DROP TABLE IF EXISTS `vaccine_locations`;

    CREATE TABLE `vaccine_locations`
    (
    `business_name` VARCHAR(77), `area_name` VARCHAR(71), `address` VARCHAR(75), `phone` VARCHAR(62), `url` VARCHAR(156), `requires_appointment` VARCHAR(53), `compass_area` VARCHAR(57), `notes` VARCHAR(261)
    )
    ;
    