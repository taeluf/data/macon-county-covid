    INSERT INTO `vaccine_locations` 
        ( `business_name`,`area_name`,`address`,`phone`,`url`,`requires_appointment`,`compass_area`,`notes` )
    VALUES
( 'MCHD', 'Mobile Clinics', '1221 E Condit St', '217-423-6988', 'https://www.facebook.com/Macon-County-Health-Department-Decatur-IL-116643335063685/', 'no', 'central', 'MCHD hosts vaccination clinics in different locations at different times and they are announced each week. You must find their post on Facebook for the latest clinic locations, or you can call them to set it up.'),
( 'Crossing Healthcare', 'North of Eldo & Water', '320 E Central Ave', '217-877-9117', 'http://www.crossinghealthcare.org/covid19-update', 'yes', 'central', 'Schedule an appointment online or by calling'),
( 'CVS', 'By Millikin', '570 N Fariview', '217-422-1570', 'https://www.cvs.com/store-locator/decatur-il-pharmacies/570-n-fairview-ave-decatur-il-62522/storeid=4857', 'no', 'west', NULL),
( 'CVS', 'By Eisenhower', '1595 E Cantrell St', '217-429-4248', 'https://www.cvs.com/store-locator/decatur-il-pharmacies/1595-e-cantrell-st-decatur-il-62521/storeid=8632', 'no', 'east', NULL),
( 'CVS ', 'in Target', '355 W Mound Rd', '217-875-6550', 'https://www.cvs.com/store-locator/decatur-il-pharmacies/355-w-mound-rd-decatur-il-62526/storeid=17068', 'no', 'north', NULL),
( 'CVS ', 'Pershing and Monroe', '2990 N Monroe St', '217-877-1703', 'https://www.cvs.com/store-locator/decatur-il-pharmacies/2990-n-monroe-street-decatur-il-62526/storeid=8648', 'no', 'central', NULL),
( 'Walgreens', 'South Shores', '420 1st Dr', '217-422-3801', 'https://www.walgreens.com/locator/walgreens-420+w+1st+dr-decatur-il-62521/id=13921', 'no', 'south', NULL),
( 'Walgreens', 'Near Macarthur', '1311 IL-48 Decatur', '217-429-1988', 'https://www.walgreens.com/locator/walgreens-1311+n+illinois+route+48-decatur-il-62526/id=5388', 'no', 'west', NULL),
( 'Walgreens', 'Pershing and Monroe', '625 W Pershing Rd', '217-875-2757', 'https://www.walgreens.com/locator/walgreens-625+w+pershing+rd-decatur-il-62526/id=9792', 'no', 'central', NULL),
( 'Walgreens', 'Walmart North', '225 E Ashe Ave', '217-872-1758', 'https://www.walgreens.com/locator/walgreens-225+e+ash+ave-decatur-il-62526/id=5545', 'no', 'north', NULL),
( 'Walgreens', 'Walmart South', '4995 E US Route 36', '217-864-9866', 'https://www.walgreens.com/locator/walgreens-4995+e+us+route+36-decatur-il-62521/id=5719', 'no', 'east', NULL),
( 'Genoa Pharmacy (Heritage)', 'Downtown on 51 South', '151 N Main St, Room 332', '217-619-0950', 'https://www.heritagenet.org/covid19', 'no', 'central', 'Walk-in Fridays 9am – 4pm. May increase hours if demand increases'),
( 'Kroger', 'South Shores', '255 W 1st Dr Decatur', '217-429-4998', 'https://www.kroger.com/stores/details/021/00924', 'no', 'south', NULL),
( 'Kroger', 'Brettwood Village', '3070 N Water St', '217-877-1327', 'https://www.kroger.com/stores/details/021/00922', 'no', 'north', NULL),
( 'Kroger', 'Walmart South', '1818 S Airport Rd', '217-864-5912', 'https://www.kroger.com/stores/details/021/00946', 'no', 'east', NULL),
( 'Colees Corner Drugs', 'Forsyth', '845 S US Route 51, Unit B', '217-330-9552', 'https://www.dalessouthlakepharmacy.com/colee-s-corner-drugs', 'no', 'north', 'Call for details or to make an appointment. Associated with Dale’s Southlake Pharmacy'),
( 'Dale’s Southlake Pharmacy', 'South Shores', '245 W 1st Drive, Suite B', '217-429-5165', 'https://www.dalessouthlakepharmacy.com/', 'idk', 'south', NULL),
( 'HSHS Medical Group', 'Behind Rural King', '5285 E Maryland St', '217-571-0510', 'https://www.hshs.org/HSHS/Find-a-Vaccine/vaccine-availability?zipcode=62522', 'yes', 'east', 'Tuesday & Thursday 1pm – 4pm'),
( 'Walmart', 'Walmart North', '4224 N Prospect', '217-875-0016', 'https://www.walmart.com/cp/immunizations-flu-shots/1228302', 'no', 'north', NULL),
( 'Walmart', 'Walmart South', '4625 E Maryland Ave', '217-864-6927', 'https://www.walmart.com/cp/immunizations-flu-shots/1228302', 'no', 'east', NULL),
( 'Sam’s Club', 'Walmart North', '4334 Prospect Dr', '217-876-9202', 'https://www.samsclub.com/pharmacy/immunization?imzType=covid', 'no', 'north', 'Sam’s Club membership NOT required')
;
