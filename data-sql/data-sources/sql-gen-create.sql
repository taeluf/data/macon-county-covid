    DROP TABLE IF EXISTS `data_sources`;

    CREATE TABLE `data_sources`
    (
    `source_name` VARCHAR(64), `dataset_name` VARCHAR(107), `purpose` VARCHAR(66), `sub_purpose` VARCHAR(105), `url_main` VARCHAR(149), `url_alt` VARCHAR(131), `url_data` VARCHAR(144), `notes` VARCHAR(220)
    )
    ;
    