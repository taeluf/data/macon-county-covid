    INSERT INTO `death_rates` 
        ( `county`,`tested_9_24`,`cases_9_24`,`deaths_9_24`,`tested_9_17`,`cases_9_17`,`deaths_9_17`,`deaths_per_7_days_9_17`,`population`,`deaths_in_7_days_per_100k`,`deaths_in_7_days_per_100k_rank` )
    VALUES
( 'Gallatin', '6,679', '755', '7', '6,416', '728', '5', '2', '4,394', '45.5', '1'),
( 'Warren', '27,541', '2,271', '61', '26,816', '2,204', '56', '5', '16,548', '30.2', '2'),
( 'Hardin', '8,454', '564', '14', '8,244', '557', '13', '1', '3,677', '27.2', '3'),
( 'Johnson', '66,012', '2,149', '25', '64,127', '2,089', '22', '3', '12,405', '24.2', '4'),
( 'Richland', '39,051', '2,644', '55', '38,062', '2,568', '52', '3', '15,149', '19.8', '5'),
( 'Cumberland', '18,138', '1,798', '25', '17,352', '1,741', '23', '2', '10,690', '18.7', '6'),
( 'Fayette', '48,555', '4,034', '59', '46,584', '3,877', '56', '3', '21,272', '14.1', '7'),
( 'Franklin', '77,869', '6,939', '99', '75,218', '6,696', '94', '5', '38,017', '13.2', '8'),
( 'Vermilion', '213,614', '13,343', '190', '207,656', '12,908', '182', '8', '73,870', '10.8', '9'),
( 'Jackson', '122,971', '7,743', '89', '119,073', '7,566', '83', '6', '55,672', '10.8', '10'),
( 'Macon', '255,180', '14,565', '244', '246,317', '14,248', '233', '11', '103,019', '10.7', '11'),
( 'Wabash', '21,853', '2,026', '16', '21,000', '1,961', '15', '1', '11,514', '8.7', '12'),
( 'Marion', '128,655', '6,581', '138', '125,833', '6,454', '135', '3', '36,853', '8.1', '13'),
( 'Iroquois', '67,627', '3,969', '73', '65,944', '3,911', '71', '2', '26,374', '7.6', '14'),
( 'Macoupin', '101,723', '6,525', '100', '99,309', '6,365', '97', '3', '44,142', '6.8', '15'),
( 'Union', '56,073', '2,973', '47', '54,987', '2,938', '46', '1', '16,295', '6.1', '16'),
( 'Knox', '144,417', '6,715', '166', '141,535', '6,528', '163', '3', '49,141', '6.1', '17'),
( 'Williamson', '153,172', '11,528', '160', '149,258', '11,206', '156', '4', '65,827', '6.1', '18'),
( 'Edgar', '21,866', '2,375', '45', '21,162', '2,298', '44', '1', '16,763', '6.0', '19'),
( 'Effingham', '61,399', '6,128', '82', '59,252', '5,889', '80', '2', '33,662', '5.9', '20'),
( 'Lee', '119,712', '4,606', '58', '117,855', '4,549', '56', '2', '33,696', '5.9', '21'),
( 'Livingston', '143,730', '5,741', '96', '140,534', '5,643', '94', '2', '35,516', '5.6', '22'),
( 'Jo Daviess', '26,259', '2,072', '26', '25,882', '2,035', '25', '1', '21,043', '4.8', '23'),
( 'Saline', '58,723', '4,140', '64', '56,601', '3,992', '63', '1', '22,895', '4.4', '24'),
( 'Rock Island', '272,824', '17,919', '348', '266,744', '17,631', '342', '6', '140,395', '4.3', '25'),
( 'Henry', '94,169', '6,256', '74', '91,226', '6,102', '72', '2', '48,601', '4.1', '26'),
( 'Ogle', '81,645', '7,180', '87', '79,310', '7,053', '85', '2', '50,367', '4.0', '27'),
( 'St. Clair', '535,108', '35,140', '571', '524,843', '34,758', '561', '10', '256,338', '3.9', '28'),
( 'Sangamon', '508,556', '24,718', '273', '494,370', '24,263', '266', '7', '192,738', '3.6', '29'),
( 'Logan', '122,112', '4,784', '74', '119,599', '4,735', '73', '1', '28,030', '3.6', '30'),
( 'Madison', '443,979', '38,345', '578', '429,569', '37,731', '569', '9', '260,846', '3.5', '31'),
( 'McDonough', '72,234', '3,566', '56', '70,152', '3,521', '55', '1', '29,078', '3.4', '32'),
( 'Randolph', '83,755', '5,321', '97', '81,165', '5,229', '96', '1', '30,992', '3.2', '33'),
( 'Bureau', '62,513', '4,291', '88', '60,994', '4,230', '87', '1', '31,966', '3.1', '34'),
( 'Morgan', '84,497', '4,889', '96', '82,533', '4,799', '95', '1', '32,920', '3.0', '35'),
( 'Fulton', '119,512', '4,915', '70', '116,025', '4,790', '69', '1', '33,360', '3.0', '36'),
( 'LaSalle', '263,208', '14,539', '278', '256,070', '14,349', '275', '3', '107,689', '2.8', '37'),
( 'Jefferson', '76,116', '6,116', '132', '74,689', '5,981', '131', '1', '37,636', '2.7', '38'),
( 'Coles', '119,532', '7,404', '113', '115,602', '7,175', '112', '1', '50,205', '2.0', '39'),
( 'Grundy', '86,916', '7,045', '82', '84,562', '6,931', '81', '1', '51,216', '2.0', '40'),
( 'DeKalb', '188,416', '11,729', '129', '183,348', '11,542', '127', '2', '105,993', '1.9', '41'),
( 'Illinois', '31,192,769', '1,612,129', '24,783', '30,395,751', '1,590,342', '24,546', '237', '12,569,321', '1.9', '42'),
( 'Kane', '906,391', '66,108', '852', '879,597', '65,399', '842', '10', '531,769', '1.9', '43'),
( 'Boone', '80,420', '7,627', '82', '78,220', '7,566', '81', '1', '53,672', '1.9', '44'),
( 'Kankakee', '303,974', '16,856', '235', '297,612', '16,523', '233', '2', '109,680', '1.8', '45'),
( 'McLean', '487,809', '22,003', '208', '471,696', '21,611', '205', '3', '169,731', '1.8', '46'),
( 'Adams', '232,111', '12,733', '148', '227,044', '12,589', '147', '1', '65,059', '1.5', '47'),
( 'Kendall', '201,750', '15,537', '107', '195,513', '15,274', '105', '2', '131,536', '1.5', '48'),
( 'Will', '1,326,773', '87,656', '1,092', '1,291,729', '86,580', '1,083', '9', '689,931', '1.3', '49'),
( 'McHenry', '466,117', '33,177', '317', '449,777', '32,773', '313', '4', '306,820', '1.3', '50'),
( 'Peoria', '429,802', '26,076', '363', '420,302', '25,738', '361', '2', '176,297', '1.1', '51'),
( 'Champaign', '2,781,837', '25,967', '184', '2,746,076', '25,601', '182', '2', '209,231', '1.0', '52'),
( 'DuPage', '1,773,307', '104,268', '1,360', '1,726,005', '103,152', '1,352', '8', '914,269', '0.9', '53'),
( 'Lake', '1,469,835', '77,364', '1,061', '1,428,825', '76,474', '1,056', '5', '690,431', '0.7', '54'),
( 'Winnebago', '541,077', '39,170', '545', '524,720', '38,682', '543', '2', '280,456', '0.7', '55'),
( 'Cook', '5,325,818', '300,122', '5,237', '5,168,405', '296,961', '5,203', '34', '5,106,780', '0.7', '56'),
( 'Alexander', '8,060', '681', '11', '7,927', '664', '11', '0', '5,201', '0.0', '57'),
( 'Bond', '46,173', '2,448', '23', '45,002', '2,422', '23', '0', '16,004', '0.0', '58'),
( 'Brown', '37,712', '1,022', '8', '36,936', '1,013', '8', '0', '6,608', '0.0', '59'),
( 'Calhoun', '7,822', '645', '2', '7,621', '638', '2', '0', '4,559', '0.0', '60'),
( 'Carroll', '26,374', '2,265', '37', '26,024', '2,239', '37', '0', '14,287', '0.0', '61'),
( 'Cass', '28,746', '2,522', '28', '28,098', '2,477', '28', '0', '11,817', '0.0', '62'),
( 'Christian', '75,844', '4,963', '80', '73,781', '4,819', '80', '0', '31,606', '0.0', '63'),
( 'Clark', '24,885', '2,535', '38', '24,004', '2,441', '38', '0', '15,123', '0.0', '64'),
( 'Clay', '25,480', '2,422', '47', '24,565', '2,333', '47', '0', '13,020', '0.0', '65'),
( 'Clinton', '91,743', '6,837', '98', '89,602', '6,764', '98', '0', '37,292', '0.0', '66'),
( 'Crawford', '56,327', '3,079', '30', '54,022', '2,989', '30', '0', '18,341', '0.0', '67'),
( 'De Witt', '35,257', '1,968', '30', '34,207', '1,927', '30', '0', '15,326', '0.0', '68'),
( 'Douglas', '45,986', '3,076', '36', '44,484', '3,033', '36', '0', '19,479', '0.0', '69'),
( 'Edwards', '6,936', '968', '15', '6,746', '948', '15', '0', '6,411', '0.0', '70'),
( 'Ford', '41,670', '2,359', '56', '40,716', '2,330', '56', '0', '12,459', '0.0', '71'),
( 'Greene', '21,720', '2,009', '39', '21,260', '1,993', '39', '0', '12,777', '0.0', '72'),
( 'Hamilton', '12,947', '1,298', '21', '12,615', '1,272', '21', '0', '8,052', '0.0', '73'),
( 'Hancock', '25,606', '2,588', '34', '24,964', '2,534', '34', '0', '17,468', '0.0', '74'),
( 'Henderson', '9,152', '661', '14', '8,950', '631', '14', '0', '6,544', '0.0', '75'),
( 'Jasper', '17,605', '1,534', '18', '17,172', '1,495', '18', '0', '9,582', '0.0', '76'),
( 'Jersey', '43,175', '3,242', '53', '42,045', '3,197', '53', '0', '21,729', '0.0', '77'),
( 'Lawrence', '85,610', '3,034', '31', '84,495', '2,946', '31', '0', '15,408', '0.0', '78'),
( 'Marshall', '27,438', '1,356', '21', '26,883', '1,337', '21', '0', '11,348', '0.0', '79'),
( 'Mason', '34,365', '2,003', '51', '33,267', '1,962', '51', '0', '13,011', '0.0', '80'),
( 'Massac', '18,649', '2,001', '44', '18,021', '1,951', '44', '0', '13,270', '0.0', '81'),
( 'Menard', '26,506', '1,608', '12', '25,896', '1,558', '12', '0', '12,010', '0.0', '82'),
( 'Monroe', '53,589', '5,101', '99', '52,386', '5,032', '99', '0', '35,165', '0.0', '83'),
( 'Montgomery', '100,028', '4,704', '76', '98,403', '4,628', '76', '0', '27,860', '0.0', '84'),
( 'Moultrie', '34,682', '2,129', '31', '33,494', '2,093', '31', '0', '14,207', '0.0', '85'),
( 'Perry', '75,574', '4,166', '71', '74,166', '4,111', '71', '0', '20,460', '0.0', '86'),
( 'Piatt', '41,461', '1,921', '14', '40,455', '1,888', '14', '0', '16,300', '0.0', '87'),
( 'Pike', '32,625', '2,580', '56', '31,688', '2,522', '56', '0', '15,493', '0.0', '88'),
( 'Pope', '5,399', '494', '5', '5,218', '472', '5', '0', '4,163', '0.0', '89'),
( 'Pulaski', '10,029', '992', '11', '9,826', '973', '11', '0', '5,137', '0.0', '90'),
( 'Putnam', '9,207', '546', '4', '8,935', '541', '4', '0', '5,745', '0.0', '91'),
( 'Schuyler', '21,239', '974', '8', '20,782', '960', '8', '0', '6,564', '0.0', '92'),
( 'Scott', '12,547', '697', '5', '12,202', '689', '5', '0', '4,999', '0.0', '93'),
( 'Shelby', '38,686', '3,173', '43', '37,368', '3,003', '43', '0', '21,450', '0.0', '94'),
( 'Stark', '13,300', '772', '27', '12,843', '760', '27', '0', '5,190', '0.0', '95'),
( 'Stephenson', '76,023', '5,497', '89', '74,309', '5,400', '89', '0', '43,900', '0.0', '96'),
( 'Tazewell', '309,721', '19,833', '326', '302,252', '19,559', '326', '0', '130,509', '0.0', '97'),
( 'Washington', '19,584', '2,066', '27', '19,165', '2,049', '27', '0', '13,663', '0.0', '98'),
( 'Wayne', '26,334', '2,760', '57', '25,486', '2,679', '57', '0', '15,993', '0.0', '99'),
( 'White', '33,685', '2,677', '30', '32,786', '2,615', '30', '0', '13,375', '0.0', '100'),
( 'Whiteside', '100,135', '7,944', '176', '97,130', '7,813', '176', '0', '54,419', '0.0', '101'),
( 'Woodford', '95,781', '5,561', '91', '94,002', '5,497', '91', '0', '38,323', '0.0', '102'),
( 'Mercer', '25,390', '1,912', '34', '24,372', '1,870', '35', '-1', '15,209', '-6.6', '103'),
( 'Chicago', '5,955,487', '315,314', '5,750', '5,795,257', '312,415', '5,722', '28', NULL, NULL, NULL),
( 'Out Of State', '211,291', '6', '0', '206,853', '8', '0', '0', NULL, NULL, NULL),
( 'Unassigned', '195,650', '181', '0', '190,826', '158', '0', '0', NULL, NULL, NULL)
;
