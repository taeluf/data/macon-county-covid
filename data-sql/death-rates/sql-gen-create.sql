    DROP TABLE IF EXISTS `death_rates`;

    CREATE TABLE `death_rates`
    (
    `county` VARCHAR(62), `tested_9_24` VARCHAR(60), `cases_9_24` VARCHAR(59), `deaths_9_24` VARCHAR(56), `tested_9_17` VARCHAR(60), `cases_9_17` VARCHAR(59), `deaths_9_17` VARCHAR(56), `deaths_per_7_days_9_17` int(10), `population` VARCHAR(60), `deaths_in_7_days_per_100k` FLOAT, `deaths_in_7_days_per_100k_rank` int(10)
    )
    ;
    