    DROP TABLE IF EXISTS `mchd_variants`;

    CREATE TABLE `mchd_variants`
    (
    `report_date` VARCHAR(58), `total_variants` int(10), `url` VARCHAR(136), `alpha` int(10), `gamma` int(10), `delta` int(10), `california` int(10), `beta` int(10), `mu` int(10), `b_1_2` int(10), `b_1` int(10)
    )
    ;
    