    DROP TABLE IF EXISTS `idph_cases`;

    CREATE TABLE `idph_cases`
    (
    `date` DATE, `tested` int(10), `cases` int(10), `deaths` int(10), `new_cases_daily` int(10), `new_weekly_cases` int(10), `new_tests_daily` int(10), `new_weekly_tests` int(10)
    )
    ;
    